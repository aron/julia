#!/bin/sh -e

PKG_JL_BASE=https://api.github.com/repos/JuliaLang/Pkg.jl/tarball/
PKG_JL_HASH=9419c70af69153cd5c4276bff90fbb135e0aa3f1

STA_JL_BASE=https://api.github.com/repos/JuliaLang/Statistics.jl/tarball/
STA_JL_HASH=a2203d3b67f7413701be5de251622cb85c9cc69d

LIBUV_BASE=https://api.github.com/repos/JuliaLang/libuv/tarball/
LIBUV_HASH=35b1504507a7a4168caae3d78db54d1121b121e1

LIBWHICH_BASE=https://api.github.com/repos/vtjnash/libwhich/tarball/
LIBWHICH_HASH=81e9723c0273d78493dc8c8ed570f68d9ce7e89e

if ! test -r $PKG_JL_HASH; then
	wget -c ${PKG_JL_BASE}${PKG_JL_HASH}
fi

if ! test -r $STA_JL_HASH; then
	wget -c ${STA_JL_BASE}${STA_JL_HASH}
fi

if ! test -r libuv-$LIBUV_HASH.tar.gz; then
	wget -c ${LIBUV_BASE}${LIBUV_HASH} -O libuv-${LIBUV_HASH}.tar.gz
fi

if ! test -r libwhich-${LIBWHICH_HASH}.tar.gz; then
	wget -c ${LIBWHICH_BASE}${LIBWHICH_HASH} -O libwhich-${LIBWHICH_HASH}.tar.gz
fi

foobar () {
	P=${1}
	URL=${2}
	if ! test -d $P; then
		mkdir -p $P
		wget -c $URL -O $P.tar.gz
		tar xvf $P.tar.gz --strip-components=1 -C $P
		rm $P.tar.gz
	fi
}

Documenter=https://github.com/JuliaDocs/Documenter.jl/archive/v0.24.4.tar.gz
foobar Documenter $Documenter

JSON=https://github.com/JuliaIO/JSON.jl/archive/v0.21.0.tar.gz
foobar JSON $JSON

DocumenterLaTeX=https://github.com/JuliaDocs/DocumenterLaTeX.jl/archive/v0.2.0.tar.gz
foobar DocumenterLaTeX $DocumenterLaTeX

DocStringExtensions=https://github.com/JuliaDocs/DocStringExtensions.jl/archive/v0.8.0.tar.gz
foobar DocStringExtensions $DocStringExtensions

Parsers=https://github.com/JuliaData/Parsers.jl/archive/v0.3.6.tar.gz
foobar Parsers $Parsers
