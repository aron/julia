Source: julia
Section: science
Homepage: https://julialang.org
Priority: optional
Standards-Version: 4.5.0
Rules-Requires-Root: no
Vcs-Git: https://salsa.debian.org/julia-team/julia.git
Vcs-Browser: https://salsa.debian.org/julia-team/julia
Maintainer: Debian Julia Team <pkg-julia-devel@lists.alioth.debian.org>
Uploaders: Peter Colberg <peter@colberg.org>,
           Graham Inggs <ginggs@debian.org>,
           Mo Zhou <lumin@debian.org>
Build-Depends: curl,
               debhelper-compat (= 12),
               dpkg-dev (>= 1.16.2~),
               libcurl4-gnutls-dev | libcurl-dev,
               libdsfmt-dev (>= 2.2.3),
               libgit2-dev (>= 0.27.0~),
               libgmp-dev,
               libmbedtls-dev,
               libmpfr-dev,
               libopenlibm-dev (>= 0.4.1+dfsg-4~) [any-i386 any-amd64 arm64 armhf mips mips64el mipsel powerpc ppc64 ppc64el],
               libpcre2-dev (>= 10.20-3~),
               libunwind8-dev [!s390x],
               libutf8proc-dev (>= 2.1.1),
               llvm-8-dev,
               openssl,
               p7zip,
               python3-minimal:native,
               unicode-data,
               gfortran,
               libblas-dev | libblas.so,
               liblapack-dev | liblapack.so,
               zlib1g-dev,
Build-Depends-Indep: fonts-lato,
                     latexmk,
                     python3-pkg-resources,
                     python3-pygments,
                     texlive,
                     texlive-extra-utils,
                     texlive-fonts-extra,
                     texlive-latex-base,
                     texlive-latex-extra,
                     texlive-latex-recommended,
                     texlive-luatex,
                     texlive-plain-generic

Package: julia
Architecture: any
Multi-Arch: foreign
Pre-Depends: ${misc:Pre-Depends}
Depends: julia-common (= ${source:Version}),
         libjulia1 (= ${binary:Version}),
         ${misc:Depends},
         ${shlibs:Depends}
Replaces: julia-common (<< 0.5.0~)
Breaks: julia-common (<< 0.5.0~)
Suggests: ess (>= 12.09-1~), julia-doc, vim-julia
Recommends: git, openssl
Description: high-performance programming language for technical computing
 Julia is a high-level, high-performance dynamic programming language for
 technical computing, with syntax that is familiar to users of other technical
 computing environments. It provides a sophisticated compiler, distributed
 parallel execution, numerical accuracy, and an extensive mathematical function
 library. The library, mostly written in Julia itself, also integrates mature,
 best-of-breed C and Fortran libraries for linear algebra, random number
 generation, FFTs, and string processing. Julia programs are organized around
 defining functions, and overloading them for different combinations of
 argument types (which can also be user-defined).
 .
 This package provides a complete Julia installation (JIT compiler, standard
 library, text-based user interface).

Package: libjulia1
Section: libs
Architecture: any
Pre-Depends: ${misc:Pre-Depends}
Replaces: julia (<< 0.5.0~), libjulia0.6, libjulia0.7
Breaks: julia (<< 0.5.0~), libjulia0.6, libjulia0.7
Depends: ${misc:Depends},
         ${shlibs:Depends}
Description: high-performance programming language for technical computing (runtime library)
 Julia is a high-level, high-performance dynamic programming language for
 technical computing, with syntax that is familiar to users of other technical
 computing environments. It provides a sophisticated compiler, distributed
 parallel execution, numerical accuracy, and an extensive mathematical function
 library. The library, mostly written in Julia itself, also integrates mature,
 best-of-breed C and Fortran libraries for linear algebra, random number
 generation, FFTs, and string processing. Julia programs are organized around
 defining functions, and overloading them for different combinations of
 argument types (which can also be user-defined).
 .
 This package provides the Julia runtime library.

Package: julia-common
Architecture: all
Multi-Arch: foreign
Depends: ${misc:Depends}
Replaces: julia (<< 0.4.1-1~)
Breaks: julia (<< 0.4.1-1~)
Recommends: julia
Description: high-performance programming language for technical computing (common files)
 Julia is a high-level, high-performance dynamic programming language for
 technical computing, with syntax that is familiar to users of other technical
 computing environments. It provides a sophisticated compiler, distributed
 parallel execution, numerical accuracy, and an extensive mathematical function
 library. The library, mostly written in Julia itself, also integrates mature,
 best-of-breed C and Fortran libraries for linear algebra, random number
 generation, FFTs, and string processing. Julia programs are organized around
 defining functions, and overloading them for different combinations of
 argument types (which can also be user-defined).
 .
 This package contains the Julia standard library and test suite.

Package: libjulia-dev
Section: libdevel
Architecture: any
Depends: libjulia1 (= ${binary:Version}), ${misc:Depends}
Description: high-performance programming language for technical computing (development)
 Julia is a high-level, high-performance dynamic programming language for
 technical computing, with syntax that is familiar to users of other technical
 computing environments. It provides a sophisticated compiler, distributed
 parallel execution, numerical accuracy, and an extensive mathematical function
 library. The library, mostly written in Julia itself, also integrates mature,
 best-of-breed C and Fortran libraries for linear algebra, random number
 generation, FFTs, and string processing. Julia programs are organized around
 defining functions, and overloading them for different combinations of
 argument types (which can also be user-defined).
 .
 This package provides the Julia runtime headers.

Package: julia-doc
Architecture: all
Multi-Arch: foreign
Section: doc
Depends: fonts-font-awesome,
         fonts-inconsolata,
         libjs-highlight.js,
         libjs-jquery,
         libjs-jquery-ui,
         libjs-lodash,
         libjs-mathjax,
         libjs-requirejs,
         node-highlight.js,
         node-normalize.css,
         ${misc:Depends}
Suggests: julia
Description: high-performance programming language for technical computing (documentation)
 Julia is a high-level, high-performance dynamic programming language for
 technical computing, with syntax that is familiar to users of other technical
 computing environments. It provides a sophisticated compiler, distributed
 parallel execution, numerical accuracy, and an extensive mathematical function
 library. The library, mostly written in Julia itself, also integrates mature,
 best-of-breed C and Fortran libraries for linear algebra, random number
 generation, FFTs, and string processing. Julia programs are organized around
 defining functions, and overloading them for different combinations of
 argument types (which can also be user-defined).
 .
 This package contains the Julia manual, which describes the language and its
 standard library. It also contains example Julia programs.
