Starting Julia REPL
-------------------

  Simply type `julia' in a shell to launch an interactive Julia session.

  Full documentation and examples are available in the julia-doc package.
  /usr/share/doc/julia/html/en/index.html

  An Emacs mode for editing Julia source files and managing interactive sessions
  is available in the ess package.

  -- Sébastien Villemot <sebastien@debian.org>, Wed, 16 Oct 2013 16:07:45 +0200


Julia and Math Kernel Library (Intel-MKL)
-----------------------------------------

  There are two ways to switch the BLAS/LAPACK implementation to MKL.
   1. Switch the libblas.so.3 and liblapack.so.3 candidate with Debian's
      alternatives system.
   2. Rebuild Julia against MKL.

  Alternatives System
  ^^^^^^^^^^^^^^^^^^^

    You can switch e.g. libblas.so.3-x86_64-linux-gnu with galternatives.

      $ sudo apt install galternatives

    When not using OpenBLAS, you might encounter the following warning,
    but it doesn't harm:

    WARNING: Error during initialization of module LinearAlgebra:
    ErrorException("could not load symbol "openblas_get_config":
    /usr/bin/../lib/x86_64-linux-gnu/julia/libblas.so: undefined symbol: openblas_get_config")

  Rebuild against MKL
  ^^^^^^^^^^^^^^^^^^^

    To rebuild Julia against MKL, set the variable CUSTOM_MKL as 1 in
    debian/rules, and rebuild the package. Please make sure that you
    have intel-mkl installed before doing the custom build.

    Brief Instruction for doing custom build against MKL:

      0. Fetch the source package of julia and enter the source tree.

      1. Install the build dependencies and helper scripts

         $ sudo apt install devscripts
         $ sudo apt build-dep julia

      2. Modify debian/rules, setting CUSTOM_MKL to 1 .

      3. Build, check, and install.

         $ debuild -j4
         $ debc
         $ sudo debi

  Known Problems about MKL
  ^^^^^^^^^^^^^^^^^^^^^^^^

    1. When MKL is installed in the build environment, this test failure
       may appear: https://github.com/JuliaLang/julia/issues/23264

  -- Mo Zhou <cdluminate@gmail.com>  Thu, 22 Sept 2018 09:00:00 +0000
